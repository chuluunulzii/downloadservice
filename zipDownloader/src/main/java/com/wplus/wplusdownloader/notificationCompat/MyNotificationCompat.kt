package com.wplus.wplusdownloader.notificationCompat

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.os.Build
import android.os.StrictMode
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.wplus.wplusdownloader.R
import com.wplus.wplusdownloader.receiver.AbortDownloadingReceiver
import com.wplus.wplusdownloader.receiver.CancelPendingReceiver
import com.wplus.wplusdownloader.util.Data
import java.net.HttpURLConnection
import java.net.URL

class MyNotificationCompat(
    private val context: Context,
    private val mIssueId: Int,
    private val mImageUrl: String,
    private val mTitle: String,
    private val mNotificationId: Int,
    private var mIssueActivity: Intent,
    private var PublicationActivity: Intent,
    private var eventHandler: EventHandler?
) {

    companion object {
        private const val MAX_PROGRESS = 100
        private const val NOTIFICATION_GROUP = "WPLUS"
        private const val CHANNEL_ID = "WPLUS_DOWNLOADER"
    }

    private var mNotificationManagerCompat: NotificationManagerCompat? = null
    private var mBuilder: NotificationCompat.Builder? = null

    init {
        mNotificationManagerCompat = NotificationManagerCompat.from(context)
        mBuilder = NotificationCompat.Builder(context, CHANNEL_ID).apply {
            setGroup(NOTIFICATION_GROUP)
            setGroupSummary(true)
            setContentTitle(mTitle)
            setDefaults(NotificationCompat.DEFAULT_LIGHTS)
            priority = NotificationCompat.PRIORITY_LOW
            setContentText(context.getText(R.string.pending))
            setContentIntent(getIssueActivityIntent())
            setSmallIcon(if (Data().notification_icon == 0) R.drawable.ic_baseline_notifications_24 else Data().notification_icon)
            setLargeIcon(getBitmapFromUrl())
            setVisibility(NotificationCompat.VISIBILITY_PUBLIC)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mBuilder?.color = context.getColor(Data().notification_color)
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel()
            }
        }
    }

    // Getters
    private fun getIssueActivityIntent(): PendingIntent? {
        mIssueActivity.apply {
            putExtra(context.getString(R.string.issue_id_key), mIssueId)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }

        return PendingIntent.getActivity(
            context,
            mIssueId,
            mIssueActivity,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    private fun getPublicationActivityIntent(): PendingIntent? {
        PublicationActivity.apply {
            putExtra(context.getString(R.string.issue_id_key), mIssueId)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }
        return PendingIntent.getActivity(
            context,
            mIssueId,
            PublicationActivity,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    private fun getAbortIntent(): PendingIntent? {
        val intent = Intent(context, AbortDownloadingReceiver::class.java)
            .apply {
                action = "com.wplus.abort"
                putExtra(context.getString(R.string.id_key), mIssueId)
            }
        return PendingIntent.getBroadcast(
            context,
            mIssueId, intent, PendingIntent.FLAG_CANCEL_CURRENT
        )
    }

    private fun getCancelIntent(): PendingIntent? {
        val intent = Intent(context, CancelPendingReceiver::class.java).apply {
            action = "com.wplus.cancel"
            putExtra(context.getString(R.string.id_key), mIssueId)
        }
        return PendingIntent.getBroadcast(
            context,
            mIssueId,
            intent,
            PendingIntent.FLAG_CANCEL_CURRENT
        )
    }

    private fun getBitmapFromUrl(): Bitmap? {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        return try {
            val url = URL(mImageUrl)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
            BitmapFactory.decodeStream(input)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    fun getNotification(): Notification? = mBuilder?.build()

    // Methods
    private fun sendNotify() {
        getNotification()?.also {
            mNotificationManagerCompat?.notify(mNotificationId, it)
        }
    }

    fun sendPending() {
        mBuilder?.apply {
            setDeleteIntent(getCancelIntent())
            setOngoing(false)
            setAutoCancel(false)
        }
        sendNotify()
    }

    fun downloading() {
        mBuilder?.apply {
            addAction(
                NotificationCompat.Action(
                    R.drawable.ic_baseline_close_24,
                    context.getString(R.string.cancel),
                    getAbortIntent()
                )
            )
            setOngoing(true)
            setAutoCancel(false)
        }
        sendNotify()
    }

    fun sendProgress(currentProgress: Int) {
        eventHandler?.downloading(mIssueId, currentProgress)

        mBuilder?.apply {
            setContentText(context.getText(R.string.downloading))
            setProgress(MAX_PROGRESS, currentProgress, false)
            setOngoing(true)
            setAutoCancel(false)
        }
        sendNotify()
    }

    fun decompressing() {
        eventHandler?.decompressStarted(mIssueId)

        mBuilder?.apply {
            setContentIntent(getIssueActivityIntent())
            setContentText(context.getString(R.string.processing))
            setOngoing(true)
            setAutoCancel(false)
            setProgress(100, 0, true)
            clearActions()
        }
        sendNotify()
    }

    fun decompressed() {
        eventHandler?.decompressCompleted(mIssueId)

        mBuilder?.apply {
            setProgress(0, 0, false)
            setContentText(context.getString(R.string.download_completed))
            setContentIntent(getPublicationActivityIntent())
            setOngoing(false)
            setAutoCancel(true)
        }
        sendNotify()
    }

    fun decompressingFailed() {
        eventHandler?.decompressFailed(mIssueId)

        mBuilder?.apply {
            setContentText(context.getString(R.string.decompress_failed))
            setProgress(0, 0, false)
            setOngoing(false)
            setAutoCancel(true)
        }
        sendNotify()
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val attributes = AudioAttributes.Builder()
            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
            .build()

        val channel = NotificationChannel(CHANNEL_ID, mTitle, NotificationManager.IMPORTANCE_LOW)
        channel.setSound(null, attributes)
        channel.enableLights(true)
        channel.enableVibration(false)
        channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

    interface EventHandler {
        fun decompressFailed(mIssueId: Int)
        fun decompressCompleted(mIssueId: Int)
        fun decompressStarted(mIssueId: Int)
        fun downloading(mIssueId: Int, currentProgress: Int)
    }
}