package com.wplus.wplusdownloader.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.wplus.wplusdownloader.R
import com.wplus.wplusdownloader.service.DownloadService

class AbortDownloadingReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val abortIntent = Intent(context, DownloadService::class.java)
        abortIntent.action = "com.wplus.abort"
        abortIntent.putExtra(context.getString(R.string.id_key), intent.getIntExtra(context.getString(R.string.id_key), 0))
        context.startService(abortIntent)
    }
}