package com.wplus.wplusdownloader.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.wplus.wplusdownloader.R
import com.wplus.wplusdownloader.service.DownloadService

class CancelPendingReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val cancelIntent = Intent(context, DownloadService::class.java)
        cancelIntent.action = "com.wplus.cancel"
        cancelIntent.putExtra(
            context.getString(R.string.id_key),
            intent.getIntExtra(context.getString(R.string.id_key), 0)
        )
        context.startService(cancelIntent)
    }
}