package com.wplus.wplusdownloader.route

import com.wplus.wplusdownloader.util.Headers

open class RouteV2 {
    protected var DOWNLOAD = Headers().domain + "mobile-app/magazine/v1/issues/"

    protected fun routePreRequestDownload(issueId: Int): String {
        return "$DOWNLOAD$issueId/download"
    }

    protected fun routeDownloadStatus(issueId: Int): String {
        return "$DOWNLOAD$issueId/download"
    }
}