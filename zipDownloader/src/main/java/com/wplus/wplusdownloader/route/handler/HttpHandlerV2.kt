package com.wplus.wplusdownloader.route.handler

import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.ANRequest
import com.androidnetworking.common.Priority
import com.wplus.wplusdownloader.route.RouteV2
import com.wplus.wplusdownloader.util.Data
import com.wplus.wplusdownloader.util.Headers
import java.util.*

class HttpHandlerV2 private constructor() : RouteV2() {

    private val headers: Map<String, String?>
        get() {
            val headers: MutableMap<String, String?> = HashMap()
            headers["Accept"] = "application/json"
            headers["Device-UUID"] = Headers().deviceUUID
            headers["Authorization"] = Headers().authorization
            headers["Old-Device-UUID"] = Headers().oldDeviceUuid
            headers["ssid"] = Headers().ssid
            headers["session_id"] = Headers().sessionId
            return headers
        }

    companion object {
        var instance: HttpHandlerV2? = null
            get() {
                if (field == null) field = HttpHandlerV2()
                return field
            }
            private set
    }

    fun postPreRequestDownloadRequest(issueId: Int, tag: String): ANRequest<*> {
        return postANRequest(routePreRequestDownload(issueId), tag)
    }

    fun getDownloadIssue(downloadUrl: String, filename: String, tag: String): ANRequest<*> {
        return downloadANRequest(downloadUrl, Data().filePath, filename, tag)
    }

    fun putDownloadStatus(issueId: Int, params: Map<String, *>, tag: String): ANRequest<*> {
        return putANRequest(routeDownloadStatus(issueId), params, tag)
    }

    private fun putANRequest(url: String?, params: Map<String, *>, tag: String): ANRequest<*> {
        return AndroidNetworking.put(url)
            .addHeaders(headers)
            .addBodyParameter(params)
            .setTag(tag)
            .setPriority(Priority.LOW)
            .build()
    }

    private fun postANRequest(url: String, tag: String): ANRequest<*> {
        return AndroidNetworking.post(url)
            .addHeaders(headers)
            .setTag(tag)
            .setPriority(Priority.LOW)
            .build()
    }

    private fun downloadANRequest(
        url: String,
        filepath: String,
        filename: String,
        tag: String
    ): ANRequest<*> {
        return AndroidNetworking.download(url, filepath, filename)
            .addHeaders(headers)
            .setTag(tag)
            .setPriority(Priority.MEDIUM)
            .build()
    }

}