package com.wplus.wplusdownloader.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.*
import android.util.SparseIntArray
import androidx.core.app.NotificationManagerCompat
import com.androidnetworking.common.ANRequest
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.wplus.wplusdownloader.R
import com.wplus.wplusdownloader.notificationCompat.MyNotificationCompat
import com.wplus.wplusdownloader.route.handler.HttpHandlerV2
import com.wplus.wplusdownloader.util.Data
import net.lingala.zip4j.ZipFile
import net.lingala.zip4j.exception.ZipException
import org.json.JSONObject
import java.io.File

class DownloadService(
    var issueIntent: Intent,
    var publicationIntent: Intent,
    var mApplicationContext: Context,
    var mData: Data,
    private var eventHandler: EventHandler?
) : Service() {

    companion object {
        private val TAG = DownloadService::class.java.simpleName
        private const val DEFAULT_TIMEOUT = 1000
        private const val MAX_PROGRESS = 100
        var queues = SparseIntArray()
    }

    private var mThread: HandlerThread? = null
    private var mServiceHandler: ServiceHandler? = null

    private var mHandlerThreadProgress: HandlerThread? = null
    private var mHandlerProgress: Handler? = null

    private var mNotificationCompat: MyNotificationCompat? = null

    private var mCurrentProgress = 0

    private var mANRequest: ANRequest<*>? = null
    private var mParams: HashMap<String, String> = HashMap()

    override fun onCreate() {
        super.onCreate()
        mThread = HandlerThread(TAG, Process.THREAD_PRIORITY_BACKGROUND).apply {
            start()
            // Get the HandlerThread's Looper and use it for our Handler
            mServiceHandler = ServiceHandler(looper)
        }

        mHandlerThreadProgress = HandlerThread(TAG, Process.THREAD_PRIORITY_BACKGROUND).apply {
            start()
            mHandlerProgress = Handler(looper)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.also {
            it.action?.also { action ->
                val issueId = it.getIntExtra(getString(R.string.id_key), 0)
                val uniqueId = queues[issueId]
                queues.delete(issueId)
                if (action == "com.wplus.abort") {
                    mANRequest?.cancel(false)
                    destroy(true, uniqueId)
                }
                eventHandler?.downloadCancelled(issueId)
            } ?: run {
                when {
                    mData.mIssueId == 0 -> {
                        stopSelf(startId)
                        eventHandler?.serviceStop()
                    }
                    queues[mData.mIssueId] > 0 -> eventHandler?.pleaseWait()
                    else -> {
                        queues.append(mData.mIssueId, startId)
                        val notificationCompat = MyNotificationCompat(
                            this@DownloadService,
                            mData.mIssueId,
                            mData.mIssueImage,
                            mData.mIssueName,
                            startId,
                            issueIntent,
                            publicationIntent,
                            object : MyNotificationCompat.EventHandler {
                                override fun decompressFailed(mIssueId: Int) {
                                    eventHandler?.inDecompressFailed(mIssueId)
                                }

                                override fun decompressCompleted(mIssueId: Int) {
                                    eventHandler?.inDecompressCompleted(mIssueId)
                                }

                                override fun decompressStarted(mIssueId: Int) {
                                    eventHandler?.inDecompressStarted(mIssueId)
                                }

                                override fun downloading(mIssueId: Int, currentProgress: Int) {
                                    eventHandler?.inDownloading(mIssueId, currentProgress)
                                }
                            })
                        notificationCompat.sendPending()

                        mServiceHandler?.obtainMessage()?.also { msg ->
                            msg.arg1 = startId
                            msg.obj = notificationCompat
                            msg.data = Bundle().apply {
                                putSerializable(getString(R.string.issue_id_key), mData.mIssueId)
                            }
                            mServiceHandler?.sendMessage(msg)
                        }
                    }
                }
            }
        }
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        mServiceHandler?.removeCallbacksAndMessages(null)
        mThread?.quitSafely()
    }

    private fun onPrerequisiteDownload() {
        val startTime = System.currentTimeMillis()
        mData.mIssueId.also {
            mANRequest = HttpHandlerV2.instance
                ?.postPreRequestDownloadRequest(it, TAG)

            val response = mANRequest?.executeForJSONObject()
            if (response?.okHttpResponse?.code() == 200) {
                val jsonObject: JSONObject = response.result as JSONObject? ?: JSONObject()
                onDownload(it, jsonObject.optString(getString(R.string.url_key)), startTime)
            } else {
                eventHandler?.anErrorHandler(
                    this,
                    response?.error?.errorCode,
                    response?.error?.errorBody
                )
            }
        }
    }

    private fun onDownload(issueId: Int, downloadUrl: String, startTime: Long) {
        mNotificationCompat?.downloading()
        mHandlerProgress?.postDelayed(object : Runnable {
            override fun run() {
                if (MAX_PROGRESS > mCurrentProgress && issueId.let { queues.get(it, 0) } != 0) {
                    mNotificationCompat?.sendProgress(mCurrentProgress)
                    mHandlerProgress?.postDelayed(this, DEFAULT_TIMEOUT.toLong())
                }
            }
        }, DEFAULT_TIMEOUT.toLong())
        mANRequest = HttpHandlerV2.instance
            ?.getDownloadIssue(downloadUrl, "${issueId}.zip", TAG)
            ?.setDownloadProgressListener { bytesDownloaded, totalBytes ->
                mCurrentProgress = (bytesDownloaded / (totalBytes / 100)).toInt()
            }

        mCurrentProgress = 100

        val response = mANRequest?.executeForDownload()
        val endTime = System.currentTimeMillis()

        mParams = hashMapOf(getString(R.string.time_key) to "${(endTime - startTime) / 1000}")

        if (response?.okHttpResponse?.code() == 200) {
            mParams[getString(R.string.status_key)] = getString(R.string.done)
            decompress()
        } else {
            val errorMessage = getString(
                R.string.download_failed_message,
                response?.error?.errorCode,
                response?.error?.message
            )
            mParams[getString(R.string.status_key)] = if (response?.error?.errorCode == 0) {
                getString(R.string.cancelled)
            } else {
                getString(R.string.failed)
            }
            sendDownloadStatus(errorMessage)
            NotificationManagerCompat.from(applicationContext).cancel(issueId)
        }
    }

    private fun decompress() {
        val startTime = System.currentTimeMillis()
        mData.mIssueId.also { issueId ->
            mNotificationCompat?.decompressing()

            val mFile = File("${mApplicationContext.filesDir.path}/library")
            if (!mFile.exists()) {
                mFile.mkdirs()
            }

            val file = File("${mFile.path}/${issueId}.zip")
            var corruptedMessage = ""

            try {
                val zipFile = ZipFile(file.path)
                zipFile.extractAll("${mFile.path}/${issueId}")
            } catch (e: ZipException) {
                corruptedMessage = e.message.toString()
                e.printStackTrace()
            } finally {
                mParams[getString(R.string.unzip_time_key)] =
                    ((System.currentTimeMillis() - startTime) / 1000).toString() + ""
                if (corruptedMessage.isEmpty()) {
                    mNotificationCompat?.decompressed()
                    eventHandler?.inDecompressed(mData.mIssueId)
                    mParams[getString(R.string.unzip_status_key)] = getString(R.string.done)
                } else {
                    mNotificationCompat?.decompressingFailed()
                    mParams[getString(R.string.unzip_status_key)] = getString(R.string.failed)
                }
                sendDownloadStatus(corruptedMessage)
                val del = file.delete()
            }
        }
    }

    private fun networkType(): String {
        val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            connMgr.activeNetwork?.let {
                connMgr.getNetworkCapabilities(it)?.let { network ->
                    when {
                        network.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> "wifi"
                        network.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> "4g"
                        network.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> "3g"
                        else -> "unknown"
                    }
                } ?: "unknown"
            } ?: "unknown"
        } else {
            connMgr.activeNetworkInfo?.let {
                when (it.type) {
                    ConnectivityManager.TYPE_WIFI -> "wifi"
                    ConnectivityManager.TYPE_MOBILE -> "4g"
                    ConnectivityManager.TYPE_ETHERNET -> "3g"
                    else -> "unknown"
                }
            } ?: "unknown"
        }
    }

    // Handler that receives messages from the thread
    private inner class ServiceHandler(looper: Looper) : Handler(looper) {
        override fun handleMessage(msg: Message) {
            var removeNotification = true
            if (mData.mIssueId.let { queues.get(it, 0) } != 0) {
                mNotificationCompat = msg.obj as MyNotificationCompat?
                mNotificationCompat?.getNotification().also {
                    startForeground(msg.arg1, it)
                }
                mCurrentProgress = 0
                onPrerequisiteDownload()
                removeNotification = false
            }

            destroy(removeNotification, msg.arg1)
        }
    }

    private fun sendDownloadStatus(errorMessage: String) {
        mParams[getString(R.string.network_type_key)] = networkType()
        mParams[getString(R.string.error_message_key)] = errorMessage
        mData.mIssueId.also {
            HttpHandlerV2.instance
                ?.putDownloadStatus(it, mParams, TAG)
                ?.getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject?) {}

                    override fun onError(anError: ANError?) {}
                })
        }
    }

    private fun destroy(removeNotification: Boolean = false, startId: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && !removeNotification)
            stopForeground(STOP_FOREGROUND_DETACH)
        else
            stopForeground(removeNotification)

        stopSelf(startId)
    }

    interface EventHandler {
        fun inDecompressed(issueId: Int)
        fun inDecompressFailed(mIssueId: Int)
        fun inDecompressCompleted(mIssueId: Int)
        fun inDecompressStarted(mIssueId: Int)
        fun inDownloading(mIssueId: Int, currentProgress: Int)
        fun anErrorHandler(context: Context, errorCode: Int?, errorBody: String?)
        fun downloadCancelled(issueId: Int)
        fun serviceStop()
        fun pleaseWait()
    }

}