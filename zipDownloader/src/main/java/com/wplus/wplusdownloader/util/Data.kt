package com.wplus.wplusdownloader.util

import com.wplus.wplusdownloader.R

class Data(
    var notification_icon: Int = 0,
    var notification_color: Int = R.color.colorAccent,
    var filePath: String = "",
    var mIssueId: Int = 0,
    var mIssueImage: String = "",
    var mIssueName: String = ""
) {

    class Builder(
        notification_icon: Int,
        notification_color: Int,
        filePath: String,
        mIssueId: Int,
        mIssueImage: String,
        mIssueName: String
    ) {
        private var notification_icon: Int = 0
        private var notification_color: Int = R.color.colorAccent
        private var filePath: String = ""
        private var mIssueId: Int = 0
        private var mIssueImage: String = ""
        private var mIssueName: String = ""

        init {
            this.notification_icon = notification_icon
            this.notification_color = notification_color
            this.filePath = filePath
            this.mIssueId = mIssueId
            this.mIssueImage = mIssueImage
            this.mIssueName = mIssueName
        }

        fun build() =
            Data(notification_icon, notification_color, filePath, mIssueId, mIssueImage, mIssueName)
    }
}