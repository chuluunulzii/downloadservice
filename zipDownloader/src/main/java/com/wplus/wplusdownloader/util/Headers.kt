package com.wplus.wplusdownloader.util

class Headers(
    var deviceUUID: String = "",
    var authorization: String = "",
    var oldDeviceUuid: String = "",
    var ssid: String = "",
    var sessionId: String = "",
    var domain: String = ""
) {

    class Builder(
        deviceUUID: String,
        authorization: String,
        oldDeviceUuid: String,
        ssid: String,
        sessionId: String,
        domain: String
    ) {
        private var deviceUUID = ""
        private var authorization = ""
        private var oldDeviceUuid = ""
        private var ssid = ""
        private var sessionId = ""
        private var domain = ""

        init {
            this.deviceUUID = deviceUUID
            this.authorization = authorization
            this.oldDeviceUuid = oldDeviceUuid
            this.ssid = ssid
            this.sessionId = sessionId
            this.domain = domain
        }

        fun build() = Headers(deviceUUID, authorization, oldDeviceUuid, ssid, sessionId, domain)
    }
}